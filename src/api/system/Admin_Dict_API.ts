import request from '/@/utils/request';
import commonUtil from '/@/utils/commonUtil'

export function api() {
	return {
		list: (params?: object) => {
			return request({
				url: commonUtil.getBaseUrl()+'/Admin_Dict/list',
				method: 'post',
				// params: params, //post请求提供此参数
				data: params, //json请求提供此参数
			});
		},
		operate: (params?: object) => {
			return request({
				url: commonUtil.getBaseUrl()+'/Admin_Dict/operate',
				method: 'post',
				data: params,
			});
		},
	};
}


