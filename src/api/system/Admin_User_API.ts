import request from '/@/utils/request';
import commonUtil from '/@/utils/commonUtil'

export function api() {
	return {
		list: (params?: object) => {
			return request({
				url: commonUtil.getBaseUrl()+'/Admin_User/list',
				method: 'post',
				// params: params, //post请求提供此参数
				data: params, //json请求提供此参数
			});
		},
		operate: (params?: object) => {
			return request({
				url: commonUtil.getBaseUrl()+'/Admin_User/operate',
				method: 'post',
				data: params,
			});
		},
		login: (params?: object) => {
			return request({
				url: commonUtil.getBaseUrl()+'/Admin_User/login',
				method: 'post',
				data: params,
			});
		},
		logout: (params?: object) => {
			return request({
				url: commonUtil.getBaseUrl()+'/Admin_User/logout',
				method: 'post',
				data: params,
			});
		},
	};
}


