import request from '/@/utils/request';
import commonUtil, {getComponetUrl} from '/@/utils/commonUtil'

export function api() {
	return {
		list: (params?: object) => {
			return request({
				url: commonUtil.getComponetUrl()+'/Component_Quartz/list',
				method: 'post',
				// params: params, //post请求提供此参数
				data: params, //json请求提供此参数
			});
		},
		operate: (params?: object) => {
			return request({
				url: commonUtil.getComponetUrl()+'/Component_Quartz/operate',
				method: 'post',
				data: params,
			});
		},
		runJob: (params?: object) => {
			return request({
				url: commonUtil.getComponetUrl()+'/Component_Quartz/runJob',
				method: 'post',
				data: params,
			});
		},
		refresh: (params?: object) => {
			return request({
				url: commonUtil.getComponetUrl()+'/Component_Quartz/refresh',
				method: 'post',
				data: params,
			});
		},

		list_log: (params?: object) => {
			return request({
				url: commonUtil.getComponetUrl()+'/Admin_QuartzLog/list',
				method: 'post',
				data: params,
			});
		},
	};
}


