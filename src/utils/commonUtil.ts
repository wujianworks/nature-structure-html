import { Session } from '/@/utils/storage';
import {ElMessage} from 'element-plus';
import {Ref} from "vue";
export const API = import.meta.env.VITE_API_URL as any;
export const QUANT_API = import.meta.env.VITE_API_QUANT_URL as any;
// export const DOMAIN = 'http://localhost:8083';
export const DOMAIN = import.meta.env.VITE_API_DOMAIN_URL as any;
// export const DOMAIN = 'https://dev.foctm.com';
// export const API = 'https://47.115.42.194/manager/api';
// export const API = 'http://localhost:8081/manager';
/**
 * 查找本地配置的base url
 * @returns base url
 */
export function getBaseUrl(): string {
    // return API;
    return DOMAIN+'/admin/v1/api';
}

export function getComponetUrl(): string {
    // return API;
    return DOMAIN+'/component/quartz/v1/api';
}

export function getEcmallUrl(): string {
    // return API;
    return DOMAIN+'/ecmall/v1/api';
}

/**
 * 查找本地配置的 quant url
 * @returns quant url
 */
export function getQuantUrl(): string {
    // return QUANT_API;
    return DOMAIN+'/quant/v1/api';
}

/**
 * 查找本地配置的 Python url
 * @returns Python url
 */
export function getPythonUrl(): string {
    // return QUANT_API;
    return DOMAIN_PYTHON+'/v1/api/quant';
}

/**
 * 获取用户信息
 * @returns 有权限，返回 `true`，反之则反
 */
export function getUserInfo(): Object {
    const userInfo = Session.get('userInfo');
    return userInfo;
}

/**
 * 获取用户id
 * @returns 有权限，返回 `true`，反之则反
 */
export function getUserId(): number {
    const userInfo = Session.get('userInfo');
    return userInfo ? userInfo.id : null;
}

/**
 * 通用处理异常的功能
 * @returns 有权限，返回 `true`，反之则反
 */
export function hasErr(result? : object | null, msg?: string | null, prompt?: boolean | null): boolean {
    if (result.status != 200) {
        ElMessage({ showClose: true, message: msg+'失败: '+result.message, type: 'error', });
        return true;
    }
    prompt && ElMessage({ showClose: true, message: msg+'成功: '+result.message, type: 'success', });
    return false;
}

/**
 * 分析并返回级联组件值, 返回值数组
 * @returns 有权限，返回 `true`，反之则反
 */
export function getCascaderValues(target?: Ref<any>): [] | null {
    if (!target) {return null;}
    let resultArr = []
    if (target) {
        let checkedNodes = target.value.getCheckedNodes(false);
        if(checkedNodes) {
            for (let i = 0; i < checkedNodes.length; i++) {
                resultArr.push(checkedNodes[i].data.id)
            }
        }
    }
    return resultArr;
}

/**
 * 分析并返回级联组件值, 返回值数组
 * @returns 有权限，返回 `true`，反之则反
 */
export function getCascaderSignalValue(target?: Ref<any>): string | null {
    let arr = getCascaderValues(target);
    return (arr!=undefined && arr?.length>0 ? arr[0] : null) ;

}

//获取url上的参数
export function getUrlParams(url?: string | null) {
    url = (url ? url : window.location.href)
    // 通过 ? 分割获取后面的参数字符串
    let urlStr = url.split('?')[1]
    // 创建空对象存储参数
    let obj = {};
    if(urlStr == undefined){return obj;}
    // 再通过 & 将每一个参数单独分割出来
    let paramsArr = urlStr.split('&')
    for(let i = 0,len = paramsArr.length;i < len;i++){
        // 再通过 = 将每一个参数分割为 key:value 的形式
        let arr = paramsArr[i].split('=')
        obj[arr[0]] = arr[1];
    }
    return obj
}

export function setUpInterval(func ?: any) {
    let intervalId = setInterval(func, 1000);
    return intervalId;
}



export default {setUpInterval,getBaseUrl,getEcmallUrl,getComponetUrl,getQuantUrl,getPythonUrl,
    getUserInfo,getUserId,getCascaderValues,getCascaderSignalValue,hasErr,getUrlParams}

